import curses
import gui
from world import World

def main(stdscreen):
    game_gui = gui.conwayGUI(stdscreen)
    my_world = None # fyll i ett funktionsanrop här
    
    game_gui.update_game(my_world)

if __name__ == "__main__":    
    curses.wrapper(main)
